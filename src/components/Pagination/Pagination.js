import React from 'react';
import {Pagination as PaginationReact, PaginationItem} from "reactstrap";
import PaginationLink from "reactstrap/es/PaginationLink";

class Pagination extends React.Component {
    calculatePagination = () => {
        const {page, total} = this.props;
        const pagination = {
            page: page || 1,
            total: total || 1,
            disabledFirst: false,
            disabledLast: false,
            breakFirst: false,
            breakLast: false,
            showFirst: false,
            showLast: false
        };

        if (pagination.total <= 0) pagination.total = 1;
        if (pagination.page <= 0) pagination.page = 1;
        if (pagination.page > pagination.total) pagination.page = pagination.total;
        if (pagination.page === 1) pagination.disabledFirst = true;
        if (pagination.page === pagination.total) pagination.disabledLast = true;

        if (pagination.page > 1 + 1) pagination.breakFirst = true;
        if (pagination.page < pagination.total - 1) pagination.breakLast = true;

        if (pagination.page > 1) pagination.showFirst = true;
        if (pagination.page < pagination.total) pagination.showLast = true;

        return pagination;
    }

    handleChange = (page) => () => {
        if (!!this.props.onChange)
            this.props.onChange(page);
    }

    render() {
        const pagination = this.calculatePagination();

        const firstPage = pagination.showFirst ?
            <PaginationItem><PaginationLink onClick={this.handleChange(1)}>1</PaginationLink></PaginationItem> : '';
        const lastPage = pagination.showLast ?
            <PaginationItem><PaginationLink
                onClick={this.handleChange(pagination.total)}>{pagination.total}</PaginationLink></PaginationItem> : '';
        const breakFirst = pagination.breakFirst ?
            <PaginationItem><PaginationLink>...</PaginationLink></PaginationItem> : '';
        const breakLast = pagination.breakLast ?
            <PaginationItem><PaginationLink>...</PaginationLink></PaginationItem> : '';


        return (
            <PaginationReact>
                <PaginationItem disabled={pagination.disabledFirst}>
                    <PaginationLink onClick={this.handleChange(pagination.page - 1)}>Prev</PaginationLink>
                </PaginationItem>
                {firstPage}
                {breakFirst}
                <PaginationItem active>
                    <PaginationLink>{pagination.page}</PaginationLink>
                </PaginationItem>
                {breakLast}
                {lastPage}
                <PaginationItem disabled={pagination.disabledLast}>
                    <PaginationLink onClick={this.handleChange(pagination.page + 1)}>Next</PaginationLink>
                </PaginationItem>
            </PaginationReact>
        );
    }
}

export default Pagination;