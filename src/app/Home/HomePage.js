import React from 'react';
import apiOrders from "../../services/orders";
import {Alert, Card, CardBody, CardHeader, Table} from "reactstrap";
import Loading from "../../components/Loading/Loading";
import TableItem from "./TableItem";
import Dashboard from "./Dashboard";

class HomePage extends React.Component {
    state = {
        products: null,
        error: '',
        isLoading: true
    }

    componentDidMount() {
        this.loadStateFromApi();
    }

    loadStateFromApi = async () => {
        try {
            const resOrders = await apiOrders.topSelling();
            if (!resOrders.data.success) throw new Error(resOrders.data.message);
            if (!resOrders.data.data) throw new Error('Response products invalid.');

            this.setState({
                products: resOrders.data.data,
                error: '',
                isLoading: false
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    renderError = () => {
        if (this.state.error) {
            return <Alert color='danger'>{this.state.error}</Alert>;
        }
    }

    renderOrdersBody = () => {
        const {products} = this.state;

        return <Table responsive>
            <thead>
            <tr>
                <th>#</th>
                <th>Thumbnail</th>
                <th>Title</th>
                <th>Price</th>
                <th>Sales</th>
                <th>Address</th>
            </tr>
            </thead>
            <tbody>
            {
                products.map((product, key) => {
                    return <TableItem key={key} index={key + 1} product={product}/>;
                })
            }
            </tbody>
        </Table>
    }

    render() {
        if (this.state.isLoading) return <Loading/>;

        return (
            <div className='HomePage'>
                {this.renderError()}

                <Dashboard/>

                <Card>
                    <CardHeader>
                        <h4 className="d-flex justify-content-between align-items-center w-100 font-weight-bold mb-3">
                            <div>Top selling foods</div>
                        </h4>
                    </CardHeader>
                    <CardBody className='TopSelling'>
                        {
                            this.renderOrdersBody()
                        }
                    </CardBody>
                </Card>
            </div>
        )
    }
}

export default HomePage;