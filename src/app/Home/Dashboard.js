import React from 'react';
import {Card, CardBody, Col, Row} from "reactstrap";

class Dashboard extends React.Component {
    render() {
        return (
            <Row className='Dashboard font-weight-bold mb-3'>
                <Col md={4}>
                    <Card className='DashboardCard'>
                        <CardBody>
                            <div className='Title'>Conversions</div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Orders</div>
                                <div>43</div>
                            </div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Unit Sales</div>
                                <div>6</div>
                            </div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Conversion Rate</div>
                                <div>23.47%</div>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
                <Col md={4}>
                    <Card className='DashboardCard'>
                        <CardBody>
                            <div className='Title'>Revenue</div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Revenue</div>
                                <div>1160.57$</div>
                            </div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Average Order Value</div>
                                <div>25.66$</div>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
                <Col md={4}>
                    <Card className='DashboardCard'>
                        <CardBody>
                            <div className='Title'>Conversion Funnel</div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>View Product</div>
                                <div>2347</div>
                            </div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Add to Cart</div>
                                <div>566</div>
                            </div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Initiate Checkout</div>
                                <div>542</div>
                            </div>
                            <div className='d-flex justify-content-between mt-3'>
                                <div>Purchase</div>
                                <div>537</div>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default Dashboard;