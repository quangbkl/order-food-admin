import React from 'react';

class TableItem extends React.Component {
    render() {
        const {product} = this.props;
        const thumbnail = Array.isArray(product.pictures) ? product.pictures[0]: null;

        return (
            <tr>
                <td>{this.props.index}</td>
                <td>
                    <img src={thumbnail} alt='thumbnail'/>
                </td>
                <td>{product.title}</td>
                <td>${product.price.toFixed(2)}</td>
                <td>{product.number_of_orders}</td>
                <td>{product.address}</td>
            </tr>
        );
    }
}

export default TableItem;