import React from 'react';
import {Redirect} from 'react-router-dom';
import {Alert, Button, Card, CardFooter, CardHeader} from "reactstrap";
import Pagination from "../../../components/Pagination/Pagination";
import apiProducts from "../../../services/products";
import Loading from "../../../components/Loading/Loading";
import queryString from 'query-string';
import ProductsBody from "./ProductsBody";
import ModalNew from "./Modals/ModalNew";
import ModalEdit from "./Modals/ModalEdit";

class ProductsPage extends React.Component {
    state = {
        products: null,
        error: '',
        isLoading: true,
        pagination: {
            page: 1,
            limit: 24,
            total: 0
        },
        redirect: false,
        modalNew: false,
        modalEdit: null
    }

    componentDidMount() {
        this.loadStateFromAll();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if ((prevState.pagination.page !== this.state.pagination.page || prevState.pagination.limit !== this.state.pagination.limit)) {
            this.setRedirect();
        }

        if (prevState.redirect !== this.state.redirect && !this.state.redirect) {
            this.loadStateFromAll();
        }
    }

    loadStateFromAll = () => {
        this.loadStateFromParams()
            .then(() => {
                this.loadStateFromApi();
            });
    }

    loadStateFromApi = async () => {
        try {
            const resProducts = await apiProducts.reads({
                page: this.state.pagination.page,
                limit: this.state.pagination.limit
            });
            if (!resProducts.data.success) throw new Error(resProducts.data.message);
            if (!resProducts.data.data) throw new Error('Response products invalid.');

            this.setState({
                products: resProducts.data.data,
                error: '',
                isLoading: false,
                pagination: Object.assign({}, this.state.pagination, {total: resProducts.data.total})
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    loadStateFromParams = () => {
        const params = this.parseParams();
        const {page: pageStr, limit: limitStr} = params;
        const page = !!pageStr ? parseInt(pageStr) : pageStr;
        const limit = !!limitStr ? parseInt(limitStr) : limitStr;

        const newPagination = {
            page: !!page && page > 0 ? page : 1,
            limit: !!limit && limit > 0 ? limit : 24
        }

        return new Promise(resolve => {
            this.setState({
                pagination: Object.assign({}, this.state.pagination, newPagination)
            }, () => {
                resolve();
            });
        });
    }

    parseParams = () => {
        const {location} = this.props;
        const parsed = queryString.parse(location.search);

        return parsed;
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            this.setState({
                redirect: false
            });

            return <Redirect to={`/foods?page=${this.state.pagination.page}&limit=${this.state.pagination.limit}`}/>
        }
    }

    renderError = () => {
        if (this.state.error) {
            return <Alert color='danger'>{this.state.error}</Alert>;
        }
    }

    changePagination = (page) => {
        const newPagination = {page};

        this.setState({
            pagination: Object.assign({}, this.state.pagination, newPagination)
        });
    }

    renderProductsBody = () => {
        if (this.state.products) {
            return <ProductsBody products={this.state.products}
                                 refresh={this.loadStateFromAll}
                                 openModalEdit={this.openModalEdit}/>;
        }
    }

    toggleModalNew = () => {
        this.setState({
            modalNew: !this.state.modalNew
        });
    }

    renderModalNew = () => {
        if (this.state.modalNew) {
            return <ModalNew isOpen={!!this.state.modalNew}
                             refresh={this.loadStateFromAll}
                             toggle={this.toggleModalNew}/>;
        }
    }

    openModalEdit = (product) => {
        this.setState({modalEdit: product});
    }

    closeModalEdit = () => {
        this.setState({modalEdit: null});
    }

    renderModalEdit = () => {
        if (this.state.modalEdit) {
            return <ModalEdit isOpen={!!this.state.modalEdit}
                              product={this.state.modalEdit}
                              refresh={this.loadStateFromAll}
                              toggle={this.closeModalEdit}/>;
        }
    }

    render() {
        if (this.state.isLoading) return <Loading/>;
        const {pagination} = this.state;
        const totalPagination = pagination.total > 0 && pagination.limit > 0 ? Math.ceil(pagination.total / pagination.limit) : 1;

        return (
            <div className='ProductsPage'>
                {this.renderRedirect()}
                {this.renderError()}
                <Card>
                    <CardHeader>
                        <Button color='primary' onClick={this.toggleModalNew}>New Food</Button>
                    </CardHeader>
                    {
                        this.renderProductsBody()
                    }
                    <CardFooter className='PaginationContainer'>
                        <Pagination page={pagination.page} total={totalPagination} onChange={this.changePagination}/>
                    </CardFooter>
                </Card>

                {this.renderModalNew()}
                {this.renderModalEdit()}
            </div>
        )
    }
}

export default ProductsPage;