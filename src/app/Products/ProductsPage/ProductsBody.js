import React from 'react';
import {CardBody} from "reactstrap";
import TableView from "./Tables/TableView";

class ProductsBody extends React.Component {
    render() {
        return (
            <CardBody className='ProductsBody'>
                <TableView {...this.props}/>
            </CardBody>
        );
    }
}

export default ProductsBody;