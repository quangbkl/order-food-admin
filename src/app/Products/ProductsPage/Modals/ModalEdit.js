import React from 'react';
import {
    Alert,
    Button,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import apiProducts from '../../../../services/products';

class ModalEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            description: '',
            price: 0,
            address: '',
            pictures: [],
            isLoadingButton: false,
            error: ''
        }
    }

    componentDidMount() {
        this.loadStateFromProps();
    }

    loadStateFromProps = () => {
        const {title, description, price, address, pictures} = this.props.product;
        this.setState({title, description, price, address, pictures});
    }


    handleChangeText = (e) => {
        const {name, value} = e.target;

        this.setState({
            [name]: value
        });
    }

    handleChangeNumber = (e) => {
        const {name, value} = e.target;

        this.setState({
            [name]: parseFloat(value)
        });
    }

    handleChangePictures = (e) => {
        const {name, value} = e.target;

        this.setState({
            [name]: value.split('\n')
        });
    }

    renderLoading = () => {
        if (this.state.isLoadingButton) {
            return <FontAwesomeIcon className='spinner ml-1' icon={faSpinner}/>;
        }
    }

    handleClickSave = async () => {
        try {
            this.setState({
                isLoadingButton: true
            });

            const {title, description, price, address, pictures} = this.state;
            const {_id} = this.props.product;
            const resUpdated = await apiProducts.update(_id, {title, description, price, address, pictures});
            const {success, message} = resUpdated.data;

            if (!success) throw new Error(message);

            this.setState({
                isLoadingButton: false,
                error: ''
            });

            this.props.toggle();
            this.props.refresh();
        } catch (e) {
            this.setState({
                isLoadingButton: false,
                error: e.message
            });
        }
    }

    renderError = () => {
        if (this.state.error) {
            return <Alert color='danger'>{this.state.error}</Alert>;
        }
    }

    render() {
        return (
            <div className='ModalEdit'>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.props.toggle}>Edit Food</ModalHeader>
                    <ModalBody>
                        {this.renderError()}
                        <Form>
                            <FormGroup row>
                                <Label for='Title' sm={2}>Title:</Label>
                                <Col sm={10}>
                                    <Input id='Title' name='title' value={this.state.title}
                                           onChange={this.handleChangeText}/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for='Description' sm={2}>Description:</Label>
                                <Col sm={10}>
                                    <Input type='textarea' id='Description' name='description'
                                           onChange={this.handleChangeText}
                                           value={this.state.description}/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for='Price' sm={2}>Price:</Label>
                                <Col sm={10}>
                                    <Input type='number' min={0} id='Price' name='price' value={this.state.price}
                                           onChange={this.handleChangeNumber}/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for='Address' sm={2}>Address:</Label>
                                <Col sm={10}>
                                    <Input id='Address' name='address' value={this.state.address}
                                           onChange={this.handleChangeText}/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for='Pictures' sm={2}>Pictures:</Label>
                                <Col sm={10}>
                                    <Input type='textarea' id='Pictures' name='pictures'
                                           value={this.state.pictures.join('\n')}
                                           onChange={this.handleChangePictures}/>
                                </Col>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>{' '}
                        <Button color="primary" onClick={this.handleClickSave} disabled={this.state.isLoadingButton}>
                            Save
                            {this.renderLoading()}
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default ModalEdit;