import React from 'react';
import {Table} from "reactstrap";
import TableItem from "./TableItem";

class TableView extends React.Component {
    render() {
        const {products, ...rest} = this.props;

        return (
            <div className='TableViewProducts'>
                <Table responsive>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        products.map((product, key) => {
                            return <TableItem {...rest} key={key} index={key + 1} product={product}/>;
                        })
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TableView;