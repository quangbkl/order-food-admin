import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import {ButtonGroup} from "reactstrap";
import apiProducts from '../../../../services/products';

class TableItem extends React.Component {
    state = {
        isDisabled: false
    }

    handleClickEdit = () => {
        const {product, openModalEdit} = this.props;
        openModalEdit(product);
    }

    handleClickDelete = async () => {
        try {
            this.setState({
                isDisabled: true
            });

            const {product} = this.props;
            const resDeleted = await apiProducts.remove(product._id);
            const {success, message} = resDeleted.data;

            if (!success) throw new Error(message);

            this.setState({
                isDisabled: false
            });
            this.props.refresh();
        } catch (e) {
            this.setState({
                isDisabled: false
            });
            console.log(e.message);
        }
    }

    render() {
        const {product} = this.props;
        const {isDisabled} = this.state;

        const opacityTr = !!isDisabled ? 0.3 : 1;
        return (
            <tr style={{opacity: opacityTr}}>
                <td>{this.props.index}</td>
                <td>
                    <img src={product.thumbnail} alt='Food'/>
                </td>
                <td>{product.title}</td>
                <td>${product.price.toFixed(2)}</td>
                <td>{new Date(product.created).toLocaleString()}</td>
                <td>
                    <ButtonGroup>
                        <FontAwesomeIcon className='IconEdit' color='blue' icon={faEdit}
                                         onClick={this.handleClickEdit}/>
                        <FontAwesomeIcon className='IconDelete ml-2' color='red' icon={faTrash}
                                         onClick={this.handleClickDelete}/>
                    </ButtonGroup>
                </td>
            </tr>
        );
    }
}

export default TableItem;