import React from 'react';
import ActionsTable from "./ActionsTable";
import {Link} from "react-router-dom";
import Badge from "reactstrap/es/Badge";

class TableItem extends React.Component {
    render() {
        const {order, ...rest} = this.props;
        const colorsStatus = {
            'processing': 'secondary',
            'shipped': 'success',
            'cancelled': 'danger'
        };

        const colorStatus = colorsStatus[order.status] || 'warning';

        return (
            <tr>
                <td>{this.props.index}</td>
                <td>
                    <Link to={'/orders/' + order._id}>
                        {order.product.title}
                    </Link>
                </td>
                <td>{order.quantity}</td>
                <td>${order.pricing_summary.total.toFixed(2)}</td>
                <td>
                    <Badge color={colorStatus}>
                        {order.status}
                    </Badge>
                </td>
                <td>{new Date(order.created).toLocaleString()}</td>
                <td>
                    <ActionsTable order={order} {...rest}/>
                </td>
            </tr>
        );
    }
}

export default TableItem;