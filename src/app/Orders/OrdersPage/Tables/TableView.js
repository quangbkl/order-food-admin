import React from 'react';
import {Table} from "reactstrap";
import TableItem from "./TableItem";

class TableView extends React.Component {
    render() {
        const {orders, ...rest} = this.props;

        return (
            <div className='TableViewProducts'>
                <Table responsive>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        orders.map((order, key) => {
                            return <TableItem {...rest} key={key} index={key + 1} order={order}/>;
                        })
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TableView;