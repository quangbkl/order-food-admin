import React from 'react';
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import apiOrders from '../../../../services/orders';

export default class ActionsTable extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    handleClickShip = async () => {
        try {
            const {order, refresh} = this.props;
            const resShipped = await apiOrders.update(order._id, {status: 'shipped'});
            const {success, message} = resShipped.data;
            if (!success) throw new Error(message);

            refresh();
        } catch (e) {
            console.log(e.message);
        }
    }

    handleClickCancel = async () => {
        try {
            const {order, refresh} = this.props;
            const resShipped = await apiOrders.update(order._id, {status: 'cancelled'});
            const {success, message} = resShipped.data;
            if (!success) throw new Error(message);

            refresh();
        } catch (e) {
            console.log(e.message);
        }
    }

    handleClickDelete = async () => {
        try {
            const {order, refresh} = this.props;
            const resShipped = await apiOrders.remove(order._id);
            const {success, message} = resShipped.data;
            if (!success) throw new Error(message);

            refresh();
        } catch (e) {
            console.log(e.message);
        }
    }

    render() {
        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle tag='div'>
                    <FontAwesomeIcon icon={faBars}/>
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem onClick={this.handleClickShip}>Shipped</DropdownItem>
                    <DropdownItem onClick={this.handleClickCancel}>Cancelled</DropdownItem>
                    <DropdownItem onClick={this.handleClickDelete}>Delete</DropdownItem>
                </DropdownMenu>
            </Dropdown>
        );
    }
}