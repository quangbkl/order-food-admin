import React from 'react';
import {CardBody} from "reactstrap";
import TableView from "./Tables/TableView";

class OrdersBody extends React.Component {
    render() {
        return (
            <CardBody className='OrdersBody'>
                <TableView {...this.props}/>
            </CardBody>
        );
    }
}

export default OrdersBody;