import React from 'react';
import {Redirect} from 'react-router-dom';
import {Alert, Card, CardFooter} from "reactstrap";
import Pagination from "../../../components/Pagination/Pagination";
import apiOrders from "../../../services/orders";
import Loading from "../../../components/Loading/Loading";
import queryString from 'query-string';
import OrdersBody from "./OrdersBody";

class OrderPage extends React.Component {
    state = {
        orders: null,
        error: '',
        isLoading: true,
        pagination: {
            page: 1,
            limit: 24,
            total: 0
        },
        redirect: false
    }

    componentDidMount() {
        this.loadStateFromAll();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if ((prevState.pagination.page !== this.state.pagination.page || prevState.pagination.limit !== this.state.pagination.limit)) {
            this.setRedirect();
        }

        if (prevState.redirect !== this.state.redirect && !this.state.redirect) {
            this.loadStateFromAll();
        }
    }

    loadStateFromAll = () => {
        this.loadStateFromParams()
            .then(() => {
                this.loadStateFromApi();
            });
    }

    loadStateFromApi = async () => {
        try {
            const resOrders = await apiOrders.reads({
                page: this.state.pagination.page,
                limit: this.state.pagination.limit
            });
            if (!resOrders.data.success) throw new Error(resOrders.data.message);
            if (!resOrders.data.data) throw new Error('Response orders invalid.');

            this.setState({
                orders: resOrders.data.data,
                error: '',
                isLoading: false,
                pagination: Object.assign({}, this.state.pagination, {total: resOrders.data.total})
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    loadStateFromParams = () => {
        const params = this.parseParams();
        const {page: pageStr, limit: limitStr} = params;
        const page = !!pageStr ? parseInt(pageStr) : pageStr;
        const limit = !!limitStr ? parseInt(limitStr) : limitStr;

        const newPagination = {
            page: !!page && page > 0 ? page : 1,
            limit: !!limit && limit > 0 ? limit : 24
        }

        return new Promise(resolve => {
            this.setState({
                pagination: Object.assign({}, this.state.pagination, newPagination)
            }, () => {
                resolve();
            });
        });
    }

    parseParams = () => {
        const {location} = this.props;
        const parsed = queryString.parse(location.search);

        return parsed;
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            this.setState({
                redirect: false
            });

            return <Redirect to={`/orders?page=${this.state.pagination.page}&limit=${this.state.pagination.limit}`}/>
        }
    }

    renderError = () => {
        if (this.state.error) {
            return <Alert color='danger'>{this.state.error}</Alert>;
        }
    }

    changePagination = (page) => {
        const newPagination = {page};

        this.setState({
            pagination: Object.assign({}, this.state.pagination, newPagination)
        });
    }

    renderOrdersBody = () => {
        if (this.state.orders) {
            return <OrdersBody orders={this.state.orders}
                               refresh={this.loadStateFromAll}/>;
        }
    }

    render() {
        if (this.state.isLoading) return <Loading/>;
        const {pagination} = this.state;
        const totalPagination = pagination.total > 0 && pagination.limit > 0 ? Math.ceil(pagination.total / pagination.limit) : 1;

        return (
            <div className='OrdersPage'>
                {this.renderRedirect()}
                {this.renderError()}
                <Card>
                    {
                        this.renderOrdersBody()
                    }
                    <CardFooter className='PaginationContainer'>
                        <Pagination page={pagination.page} total={totalPagination} onChange={this.changePagination}/>
                    </CardFooter>
                </Card>
            </div>
        )
    }
}

export default OrderPage;