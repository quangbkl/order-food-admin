import React from 'react';
import {Button, CardBody, CardHeader, CardTitle, Col, Row, Table} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPrint} from "@fortawesome/free-solid-svg-icons";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

class ViewDetails extends React.Component {
    constructor(props) {
        super(props);

        this.refInvoice = React.createRef();
    }

    handlePrint = () => {
        const filename = `invoice-order-food-${this.props.order._id}.pdf`;

        html2canvas(this.refInvoice.current).then(canvas => {
            let pdf = new jsPDF('p', 'mm', 'a4');
            pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);
            pdf.save(filename);
        });
    }

    render() {
        const {order} = this.props;

        return (
            <div className='ViewDetails'>
                <CardHeader className='bg-transparent header-elements-inline'>
                    <CardTitle>Static invoice</CardTitle>
                    <div className='HeaderElements'>
                        <Button color='light' onClick={this.handlePrint}>
                            <FontAwesomeIcon icon={faPrint}/>
                            Print
                        </Button>
                    </div>
                </CardHeader>
                <div ref={this.refInvoice}>
                    <CardBody>
                        <Row className='ShippingAddress'>
                            <Col sm={6}>
                                <p>{order.ship_to.first_name + ' ' + order.ship_to.last_name}</p>
                                <p>{order.ship_to.contact_address.address}</p>
                                <p>{order.ship_to.contact_address.address2}</p>
                                <p>{order.ship_to.contact_address.city + ' ' + order.ship_to.contact_address.province + ' ' + order.ship_to.contact_address.zip_code}</p>
                            </Col>
                            <Col sm={6} className='InvoiceDetails text-sm-right'>
                                <h4 className='text-primary'>Invoice #49029</h4>
                                <p>Date: <span
                                    className="font-weight-semibold">{new Date(order.created).toDateString()}</span></p>
                            </Col>
                        </Row>
                    </CardBody>
                    <Table responsive className='ListItems'>
                        <thead>
                        <tr>
                            <th>Foods</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h6>{order.product.title}</h6>
                                <span className="text-muted">{order.product.address}</span>
                            </td>
                            <td>${order.product.price.toFixed(2)}</td>
                            <td>{order.quantity}</td>
                            <td><span className="font-weight-semibold">${order.pricing_summary.total.toFixed(2)}</span></td>
                        </tr>
                        </tbody>
                    </Table>

                    <CardBody>
                        <div className="FooterInvoice">
                            <div className="AuthorizedPerson">
                                <h6 className="mb-3">Authorized person</h6>
                                <ul className="list-unstyled text-muted">
                                    <li>{order.ship_to.first_name + ' ' + order.ship_to.last_name}</li>
                                    <li>{order.ship_to.company}</li>
                                    <li>{order.ship_to.email}</li>
                                    <li>{order.ship_to.phone_number}</li>
                                </ul>
                            </div>

                            <div className="TotalDue">
                                <h6 className="mb-3">Total due</h6>
                                <Table responsive>
                                    <tbody>
                                    <tr>
                                        <th>Subtotal:</th>
                                        <td className="text-right">${order.pricing_summary.subtotal.toFixed(2)}</td>
                                    </tr>
                                    <tr>
                                        <th>Tax:</th>
                                        <td className="text-right">${order.pricing_summary.tax.toFixed(2)}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td className="text-right text-primary"><h5
                                            className="font-weight-semibold">${order.pricing_summary.total.toFixed(2)}</h5></td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </CardBody>
                </div>
            </div>
        );
    }
}

export default ViewDetails;