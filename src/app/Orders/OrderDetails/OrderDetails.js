import React from 'react';
import {Alert, Card, CardBody} from "reactstrap";
import apiOrders from '../../../services/orders/index';
import ViewDetails from "./ViewDetails";
import Loading from "../../../components/Loading/Loading";

class OrderDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            order: null,
            isLoading: true,
            error: ''
        }
    }

    componentDidMount() {
        this.loadStateFromApi();
    }

    loadStateFromApi = async () => {
        try {
            const orderId = this.props.match.params.orderId;
            const resOrder = await apiOrders.read(orderId);
            if (!resOrder.data.success) throw new Error(resOrder.data.message);

            this.setState({
                order: resOrder.data.data,
                error: '',
                isLoading: false
            });
        } catch (e) {
            this.setState({
                error: e.message,
                isLoading: false
            });
        }
    }

    render() {
        if (this.state.isLoading) return <Loading/>;

        const errorComponent = !!this.state.error ?
            <CardBody><Alert color='danger'>{this.state.error}</Alert></CardBody> : '';
        const invoiceDetails = !!this.state.order ?
            < ViewDetails order={this.state.order}/> : '';

        return (
            <Card className='OrderDetails'>
                {errorComponent}
                {invoiceDetails}
            </Card>
        );
    }
}

export default OrderDetails;