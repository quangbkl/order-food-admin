import React, {Component} from 'react';
import './scss/style.scss';
import './css/bootstrap.min.css';
import './css/bootstrap_limitless.min.css';
import './css/colors.min.css';
import './css/components.min.css';
import './css/layout.min.css';
import Header from './layout/Header/Header';
import Main from './layout/Main/Main';
import Footer from "./layout/Footer/Footer";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <Main/>
                <Footer/>
            </div>
        );
    }
}

export default App;
