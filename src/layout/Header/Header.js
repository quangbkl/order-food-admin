import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    InputGroup, Input, InputGroupAddon, Button
} from 'reactstrap';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import logo from '../../assets/logo.png';
import {Link} from "react-router-dom";

class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <Navbar light expand="md">
                <NavbarBrand>
                    <Link to='/'>
                        <img src={logo} alt='logo'/>
                    </Link>
                </NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <Link className='nav-link' to="/foods">Foods</Link>
                        </NavItem>
                        <NavItem>
                            <Link className='nav-link' to="/orders">Orders</Link>
                        </NavItem>
                        <NavItem>
                            <form className='FormSearch'>
                                <InputGroup>
                                    <Input type='text'/>
                                    <InputGroupAddon addonType="append">
                                        <Button>
                                            <FontAwesomeIcon icon={faSearch}/>
                                        </Button>
                                    </InputGroupAddon>
                                </InputGroup>
                            </form>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}

export default Header;