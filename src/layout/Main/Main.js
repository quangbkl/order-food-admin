import React, {Component} from 'react';
import {Route} from 'react-router-dom'
import {Container} from "reactstrap";
import HomePage from "../../app/Home/HomePage";
import ProductsPage from "../../app/Products/ProductsPage/ProductsPage";
import OrderPage from "../../app/Orders/OrdersPage/OrdersPage";
import OrderDetails from "../../app/Orders/OrderDetails/OrderDetails";

class Main extends Component {
    render() {
        return (
            <div className='Main'>
                <Container className='Container'>
                    <Route exact path="/" component={HomePage}/>
                    <Route path='/foods' component={ProductsPage}/>
                    <Route exact path='/orders' component={OrderPage}/>
                    <Route path='/orders/:orderId' component={OrderDetails}/>
                </Container>
            </div>
        );
    }
}

export default Main;