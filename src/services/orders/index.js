import axios from 'axios'
import configServer from '../../config/server';

export const reads = ({page, limit}) => {
    return axios.get(configServer.url + '/orders', {params: {page, limit}});
}

export const read = (id) => {
    return axios.get(configServer.url + '/orders/' + id)
}

export const create = (data) => {
    return axios({
        url: configServer.url + '/orders',
        method: 'post',
        data
    });
}

export const update = (id, data) => {
    return axios({
        url: configServer.url + '/orders/' + id,
        method: 'put',
        data
    });
}

export const remove = (id) => {
    return axios({
        url: configServer.url + '/orders/' + id,
        method: 'delete'
    });
}

export const topSelling = () => {
    return axios.get(configServer.url + '/top_selling');
}

export default {reads, read, create, update, remove, topSelling};