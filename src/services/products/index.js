import axios from 'axios'
import configServer from '../../config/server';

export const reads = ({page, limit}) => {
    return axios.get(configServer.url + '/products', {params: {page, limit}});
}

export const read = (id) => {
    return axios.get(configServer.url + '/products/' + id)
}

export const create = (data) => {
    return axios({
        url: configServer.url + '/products',
        method: 'post',
        data
    });
}

export const update = (id, data) => {
    return axios({
        url: configServer.url + '/products/' + id,
        method: 'put',
        data
    });
}

export const remove = (id) => {
    return axios({
        url: configServer.url + '/products/' + id,
        method: 'delete'
    });
}

export default {reads, read, create, update, remove};